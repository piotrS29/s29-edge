// S29 components
#include <s29_global.h>

/*!
 * @brief Get the current high-res monotonic time
 *
 *  Uses the FreeRtos system tick. Accumulates time
 *  across tick rollover (about 49 days for 1ms ticks)
 *
 * @return double Monotonic time in seconds as a double
 */
double S29_Utility_c::getSystemTime( void )
{
    static double systemTicksMs = 0.0;
    static TickType_t previousCountMs = 0U;

    const TickType_t currentCountMs = xTaskGetTickCount();

    // Calculate the time since the last call. This will work even if the
    //  time wraps, as we are subtracting unsigned number and the current time
    //  always is the same or larger than the previous time.
    // This value will overflow every 49 days.
    const TickType_t elapsedTimeBetweenCalls = currentCountMs - previousCountMs;
    previousCountMs = currentCountMs;

    // Accumulate the elapsed time. System ticks
    //  will not overflow in our lifetime.
    systemTicksMs += (double)elapsedTimeBetweenCalls;

    const double tickRateHz = (double)configTICK_RATE_HZ;
    // Return value in seconds
    return (systemTicksMs / tickRateHz);
}
