#ifndef _S29_UX_EDGE_H_
#define _S29_UX_EDGE_H_

// S29 components
#include <s29_global.h>

class S29_UX_EDGE_c : public S29_RealTimeFiniteStateMachine_c {
//
public:

    S29_UX_EDGE_c(  ) : S29_RealTimeFiniteStateMachine_c( s29_UxEdge_e )
                                                 ,m_initialized( false ) {
    }

    ~S29_UX_EDGE_c(  ) {
        Exit(  );
    }

    bool Initialize( void );

    virtual void Enter( void );
    virtual void Run( const double timeNow );
    virtual void Exit( void );

private:

    bool         m_initialized;
    S29_UDP_c          m_uxUDP;

    void Rx( void );
    void Tx( void );

#if 0
	void ParseRx(
	    char *       rxBuff,
	    const char * objKey
	);

	void FormatTx(
	    const char *     objKey,
	    const char * keyStrings[  ],
	    const int      keyCount,
	    bool         valueArray[  ]
	);

	void FormatTxJson(
	    const char *     objKey,
	    const char * keyStrings[  ],
	    const int      keyCount,
	    uint32_t          valueArray[  ]
	);

    #endif

	void ManualSend ( void );

};

#endif /* of _S29_UX_EDGE_H_ */
