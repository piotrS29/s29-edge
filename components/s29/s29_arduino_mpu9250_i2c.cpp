/******************************************************************************
arduino_mpu9250_i2c.cpp - MPU-9250 Digital Motion Processor Arduino Library
Jim Lindblom @ SparkFun Electronics
original creation date: November 23, 2016
https://github.com/sparkfun/SparkFun_MPU9250_DMP_Arduino_Library

This library implements motion processing functions of Invensense's MPU-9250.
It is based on their Emedded MotionDriver 6.12 library.
	https://www.invensense.com/developers/software-downloads/

Development environment specifics:
Arduino IDE 1.6.12
SparkFun 9DoF Razor IMU M0

Supported Platforms:
- ATSAMD21 (Arduino Zero, SparkFun SAMD21 Breakouts)
******************************************************************************/
//s29 #include "arduino_mpu9250_i2c.h"
//s29 #include <Arduino.h>
//s29 #include <Wire.h>
#include <arduino_mpu9250_i2c.h>
#include <s29_global.h>

#define S29_MAX_I2C_OUT_DATA_LEN    (32)

uint8_t  i2c_data_buf[ S29_MAX_I2C_OUT_DATA_LEN ];

int arduino_i2c_write(unsigned char slave_addr, unsigned char reg_addr,
                       unsigned char length, unsigned char * data)
{
  int retVal;
  size_t wlen= ( length < S29_MAX_I2C_OUT_DATA_LEN )
             ?   length
             :   S29_MAX_I2C_OUT_DATA_LEN;

  i2c_data_buf[ 0 ]= reg_addr;

  for( int i = 0; i < wlen; ++i ) {
    i2c_data_buf[ i + 1 ] = data[ i ];
  }

  retVal = s29_i2c.write( ( slave_addr << 1 ),
                            &i2c_data_buf[ 0 ],
                                  ( wlen + 1 ),
                                        false );

//  ESP_LOGI( "MPU9250", "arduino_i2c_write -> sAddr = 0x%x, rAddr = 0x%x, length = %d, data = 0x%x retVal = %d",
//                                             slave_addr, reg_addr, wlen, data[0], retVal );

#if 0 //s29
	Wire.beginTransmission(slave_addr);
	Wire.write(reg_addr);
	for (unsigned char i = 0; i < length; i++)
	{
		Wire.write(data[i]);
	}
	Wire.endTransmission(true);
#endif //s29
	return retVal;
}

int arduino_i2c_read(unsigned char slave_addr, unsigned char reg_addr,
                       unsigned char length, unsigned char * data)
{
  int retVal = 1;
  data[0] = 0xff;
  i2c_data_buf[ 0 ]= reg_addr;

  if ( s29_i2c.write( ( slave_addr << 1 ), &i2c_data_buf[ 0 ], 1, true ) ==0 ) {
    retVal = s29_i2c.read( ( ( slave_addr << 1 ) | 1 ), data, length, false );
  }

//  ESP_LOGI( "MPU9250", "arduino_i2c_read -> sAddr = 0x%x, rAddr = 0x%x, length = %d, data = 0x%x retVal = %d",
//                                            slave_addr, reg_addr, length, data[0], retVal );


#if 0
  return( (  ( s29_i2c.write( ( slave_addr << 1 ), &i2c_data_buf[ 0 ], 1, true ) ==0 )
          && ( s29_i2c.read( ( ( m_dev_addr << 1 ) | 1 ), bdata, blen, false ) == 0 ) )
        ? true
        : false );
#endif

#if 0 //s29
	Wire.beginTransmission(slave_addr);
	Wire.write(reg_addr);
	Wire.endTransmission(false);
	Wire.requestFrom(slave_addr, length);
	for (unsigned char i = 0; i < length; i++)
	{
		data[i] = Wire.read();
	}
#endif //s29
	return retVal;
}
