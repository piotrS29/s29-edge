#ifndef _S29_I2C_DEV_H_
#define _S29_I2C_DEV_H_

#include <s29_global.h>

#define S29_MAX_I2C_OUTBOUND_DATA_LEN    (32)

class S29_I2C_DEV_c {

public:

	S29_I2C_DEV_c( const uint8_t addr, S29_ESP32_I2C_c * i2c )
	    : m_dev_addr( addr )
	    , m_i2c( i2c )
	    {
	}

	virtual ~S29_I2C_DEV_c(  ) {  }

//protected:

	bool write_buf(
	    uint8_t   baddr,
	    uint8_t * bdata,
	    size_t     blen
	);

	bool write_reg(
	    uint8_t raddr,
	    uint8_t rdata
	);

	bool read_buf(
	    uint8_t   baddr,
	    uint8_t * bdata,
	    size_t     blen
	);

	bool read_reg(
	    uint8_t   raddr,
	    uint8_t * rdata
	);

private:

    uint8_t           m_dev_addr;
    S29_ESP32_I2C_c * m_i2c;
	uint8_t           m_i2c_data[ S29_MAX_I2C_OUTBOUND_DATA_LEN ];

};

#endif // of _S29_I2C_DEV_H_
