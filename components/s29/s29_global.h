#ifndef _S29_GLOBAL_H_
#define _S29_GLOBAL_H_
//
// S29 components
#include <s29_common.h>
#include <s29_rtfsm.h>
#include <s29_udp.h>
#include <s29_ux_edge.h>
#include <s29_app.h>
#include <s29_dm.h>
#include <s29_esp32_i2c.h>
#include <s29_io_edge.h>
#include <s29_utility.h>
#include <s29_vio.h>
//
// S29 components
extern class S29_MAIN_APP_c  s29_MainApp;
extern class S29_ESP32_I2C_c     s29_i2c;
extern class S29_IO_EDGE_c   s29_io_edge;
//
#endif // of _S29_GLOBAL_H_
