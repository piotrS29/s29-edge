// S29 components
#include <s29_global.h>
#include <s29_wifi.h>
//
static const char * _tag= "s29-UX";
//
static const uint16_t  txMaxLen=  512;
static const uint16_t  rxMaxLen= 1024;
//
static uint64_t  lastMpuTime= 0;
//
bool S29_UX_EDGE_c::Initialize(
    void
) {
    //	
    char flushBuff[ rxMaxLen ];
    //
    ESP_LOGI( _tag, "Initialize starting" );
    //
    m_initialized= m_uxUDP.Open(  );
    //
    // Flush out this UDP socket
    while( m_uxUDP.Receive( flushBuff, (int32_t)sizeof( flushBuff ) ) > 0 );
    //
    return( m_initialized );
}
//
void S29_UX_EDGE_c::Enter(
    void
) {
    //	
    ESP_LOGI( _tag, "Enter starting" );
    return;
}
//
void S29_UX_EDGE_c::Run(
    const double timeNow
) {
//    ESP_LOGI( _tag, "Running" );
    Rx(  );
    Tx(  );
    return;
}
//
void S29_UX_EDGE_c::Exit(
    void
) {
    //
    ESP_LOGI( _tag, "Exiting now" );
    //
    if( m_initialized ) {
        //
        m_uxUDP.Close(  );
    }
    //
    return;
}
//
void S29_UX_EDGE_c::Rx(
    void
) {
	//
    char   rxBuff[ rxMaxLen ];
    //
    // Receive Data Model
    //
    const int32_t rxLen= m_uxUDP.Receive( rxBuff, (int32_t)sizeof( rxBuff ) );
    //
    if( rxLen > 0 ) {
        //
        char * ret;
        //
        rxBuff[ rxLen ]= 0;
//        ESP_LOGI( _tag, "Message Received -> rxBuff = %s", rxBuff );
        //
		ret= strstr( rxBuff, "ux" );
        //
		if( ret ) {
            //
            ret= strstr( rxBuff, S29_VIO_c::vEdgeIPaddressOut(  ) );
            //
            if( ret ) {

		        ret= strstr( rxBuff, "ledB" );
                //
		        if( ret ) {
                    //
                    int ledVal;
                    //
			        sscanf( ret, "ledB\":%d", &ledVal ); 
                    //
//                    ESP_LOGI( _tag, "ledVal = %d", ledVal );
                    //
                    S29_VIO_c::vEdgeBlueLedIn( ( ledVal == 1 ) ? true : false );
			    }
            }
		}
        //
		ret=strstr(rxBuff,"hub");
        //
		if( ret ) {
            //
		    ret=strstr( rxBuff, "click" );
            //
		    if( ret ) {
                //
                char clickVal;
                //
			    sscanf( ret, "click\":\"%c", &clickVal ); 
                //
//			    ESP_LOGI( _tag, "clickVal = %c", clickVal );
                //
                switch( clickVal )
			    {
				  case 'r':
                      S29_VIO_c::vHubRFScountIn( S29_VIO_c::vHubRFScountOut(  ) + 1 );
                      S29_VIO_c::vEdgeOrangeLedIn( !S29_VIO_c::vEdgeOrangeLedOut(  ) );
					  break;
			  
				  case 'l':
                      S29_VIO_c::vHubLFScountIn( S29_VIO_c::vHubLFScountOut(  ) + 1 );
					  break;

				  default:
					  break;
			    }
			}
		}
	}
	return;
}

void S29_UX_EDGE_c::Tx(
    void
) {
    //
    char   txBuff[ txMaxLen ];
    //
    // Transmit Data Model
    //
    if( strcmp( S29_VIO_c::vEdgeIPaddressOut(  ), LOCALHOST_IP ) != 0
     && S29_VIO_c::vEdgeUDPcountOut(  ) == 0 ) {
        //
        sprintf( txBuff, "{\"S29\":{\"IP\":\"%s\",\"count\":%d}}", S29_VIO_c::vEdgeIPaddressOut(  ), -1 );
//    	ESP_LOGI( _tag, "sending -> %s", txBuff );
        m_uxUDP.Send( txBuff, strlen( txBuff ) );

        S29_VIO_c::vEdgeUDPcountIn( S29_VIO_c::vEdgeUDPcountOut(  ) + 1 );
    }
    //
    if( S29_VIO_c::vEdgeUDPcountOut(  ) != 0 ) {
        //
        if( lastMpuTime != S29_VIO_c::vMotionMPUtimeOut(  ) ) {
            //
            char * cJsonPrint= cJSON_PrintUnformatted( dmRoot );
            //
//            ESP_LOGI( _tag, "sending to %s:%d -> %s", _CONFIG_UDP_HUB_IP, CONFIG_UDP_TX_PORT, cJsonPrint );
            //
            lastMpuTime= S29_VIO_c::vMotionMPUtimeOut(  );
            //
            S29_VIO_c::vEdgeUDPcountIn( S29_VIO_c::vEdgeUDPcountOut(  ) + 1 );
            m_uxUDP.Send( cJsonPrint, strlen( cJsonPrint ) );
            free( cJsonPrint );
        }
    }
    //
    return;
}
