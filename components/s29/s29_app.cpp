// S29 components
#include <s29_global.h>
//
static const char     * _tag=     "s29-App";
//
static const uint16_t   APP_TICK=      5000;
//
volatile bool           run=           true;
//
static S29_UX_EDGE_c app_ux_edge;
//
void S29_MAIN_APP_c::Setup(
    void
) {
    do {
        //
        ESP_LOGI( _tag, "Setup starting" );
        //
        if( ! app_ux_edge.Initialize(  ) ) {
            //
            ESP_LOGI( _tag, "Application uxEdge initialization failed" );
            break;
        }
        //
        app_ux_edge.Enter(  );
        //
//        s29_io_map.Initialize(  );
        //
	    s29_io_edge.Open(  );
        //
        ESP_LOGI( _tag, "Setup waiting for connection" );
        //
        while( strcmp( S29_VIO_c::vEdgeIPaddressOut(  ), LOCALHOST_IP ) == 0 ) {
            //
            vTaskDelay( pdMS_TO_TICKS( 10U ) );
        }
        //
        ESP_LOGI( _tag, "Setup finished" );
        //
    }
    while( false );
    //
    return;
}
//
void S29_MAIN_APP_c::Loop(
    void
) {
    //
    void (S29_MAIN_APP_c::*objLoopPtr)(void *)= &S29_MAIN_APP_c::m_Loop;
    //
    xTaskCreate( (void (*)(void *))(s29_MainApp.*objLoopPtr),
                                                  "app_task",
                                                        4096,
                                                        NULL,
                                                           5,
                                                        NULL
    );
    //
    return;
}
//
void S29_MAIN_APP_c::m_Loop(
    void * // params
) {
    //
    ESP_LOGI( _tag, "Loop starting" );
    //
    double timeInLoop=                              0.0;
    double timeNow=                                 0.0;
    double timeBefore= S29_Utility_c::getSystemTime(  );
    //
    do {
        //        
        timeNow= S29_Utility_c::getSystemTime(  );
        //
        // Calculate loop timing statistics, max, min
        timeInLoop= timeNow - timeBefore;
        //
//        if (timeInLoop < 0.100)
//        {
//            // Delay 10ms, and try again
//            vTaskDelay(pdMS_TO_TICKS(10U));
//            continue;
//        }
        //
        timeBefore= timeNow;
        //
        S29_VIO_c::vSystemTimeIn( timeNow );
        //
        s29_io_edge.Read(  );
        //
	    app_ux_edge.Run( timeNow );
        //
        s29_io_edge.Write(  );
        //
//        vTaskDelay( pdMS_TO_TICKS( APP_TICK ) );
        //
	}
	while( run );
    //
	return;
}
