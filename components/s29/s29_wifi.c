// S29 components
#include <s29_common.h>
#include <s29_wifi.h>
//
// Espressif infrastructure
#include "esp_wifi.h"
//
/* FreeRTOS event group to signal when the device is connected */
static EventGroupHandle_t s_wifi_event_group;
//
/* The event group allows multiple bits for each event,
 * but we only care about one event
 * - are we connected to the AP with an IP ?
 */
static const int WIFI_CONNECTED_BIT= BIT0;
//
static const char * _tag= "s29-WiFi";
//
static int s_retry_num= 0;
//
static void event_handler(
    void             *        arg,
    esp_event_base_t   event_base, 
    int32_t              event_id,
    void             * event_data
) {
    if( event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START ) {
        //
        esp_wifi_connect(  );
    }
    else if( event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED ) {
        //
        if( s_retry_num < CONFIG_WIFI_MAXIMUM_RETRY ) {
            //
            esp_wifi_connect(  );
            xEventGroupClearBits( s_wifi_event_group, WIFI_CONNECTED_BIT );
            s_retry_num++;
            ESP_LOGI( _tag,"Retry (%d) to connect to the AP", s_retry_num );
        }
        //
        ESP_LOGI( _tag,"Connect to the AP fails after %d retries\n", s_retry_num );
    }
    else if( event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP ) {
        //
        ip_event_got_ip_t * event = (ip_event_got_ip_t *)event_data;

        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                EDGE_IP_KEY, cJSON_CreateString( ip4addr_ntoa( &event->ip_info.ip ) ) );
        //
        ESP_LOGI( _tag, "Got ip: %s", ( cJSON_GetObjectItem(
                                            cJSON_GetObjectItem(
                                                dmRoot, DATA_MODEL_KEY ), EDGE_IP_KEY ) -> valuestring
                                      )
        );
        s_retry_num = 0;
        xEventGroupSetBits( s_wifi_event_group, WIFI_CONNECTED_BIT );
    }
    return;
}
//
void s29_WiFi_Setup(
    void
) {
    //
    ESP_LOGI( _tag, "WiFi station MODE starting" );
    //
    s_wifi_event_group = xEventGroupCreate(  );
    //
    tcpip_adapter_init(  );
    //
    ESP_ERROR_CHECK( esp_event_loop_create_default(  ) );
    //
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT(  );
    //
    ESP_ERROR_CHECK(esp_wifi_init( &cfg ) );
    //
    ESP_ERROR_CHECK( esp_event_handler_register( WIFI_EVENT, ESP_EVENT_ANY_ID, &event_handler, NULL ) );
    ESP_ERROR_CHECK( esp_event_handler_register( IP_EVENT, IP_EVENT_STA_GOT_IP, &event_handler, NULL ) );
    //
    wifi_config_t wifi_config= {
        .sta= {
            .ssid=     _CONFIG_WIFI_SSID,
            .password= _CONFIG_WIFI_PSWD
        },
    };
    //
    ESP_ERROR_CHECK( esp_wifi_set_mode( WIFI_MODE_STA ) );
    ESP_ERROR_CHECK( esp_wifi_set_config( ESP_IF_WIFI_STA, &wifi_config ) );
    ESP_ERROR_CHECK( esp_wifi_start(  ) );
    //
    ESP_LOGI( _tag, "WiFi_Setup finished" );
    ESP_LOGI( _tag, "Connecting to AP ssid: %s password: %s",
                     _CONFIG_WIFI_SSID, _CONFIG_WIFI_PSWD );
    return;
}
