#ifndef _S29_WIFI_H_
#define _S29_WIFI_H_
//
#ifdef __cplusplus
extern "C" {
#endif
//
void s29_WiFi_Setup(
	void
);
//
#ifdef __cplusplus
}
#endif
//
#endif // of _S29_WIFI_H_
