#ifndef _S29_APP_H_
#define _S29_APP_H_
//
// S29 components
#include <s29_global.h>
//
extern volatile bool run;
//
class S29_MAIN_APP_c : public S29_RealTimeFiniteStateMachine_c {
//
public:
    //
    S29_MAIN_APP_c(  ) : S29_RealTimeFiniteStateMachine_c( s29_MainApp_e ) {
    }
    //
    ~S29_MAIN_APP_c(  ) {
    }
    //
    void Setup(
        void
    );
    //
    void Loop(
        void
    );
    //
private:
    //
    void m_Loop(
        void * //params
    );
    //
};
//
#endif // of _S29_APP_H_
