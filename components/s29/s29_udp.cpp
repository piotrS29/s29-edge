// S29 components
#include <s29_global.h>

static const char * _tag= "s29-UDP";

int32_t  S29_UDP_c::get_socket_error_code(
    int32_t socket
) {
    int32_t result;
    u32_t optlen = sizeof(int);
    if( getsockopt( socket, SOL_SOCKET, SO_ERROR, &result, &optlen) == -1 ) {
        ESP_LOGE( _tag, "getsockopt failed" );
        return( -1 );
    }
    return( result );
}

int32_t  S29_UDP_c::show_socket_error_reason(
    int32_t socket
) {
    int32_t err= get_socket_error_code( socket );
    ESP_LOGW( _tag, "socket error %d %s", err, strerror( err ) );
    return( err );
}

int32_t  S29_UDP_c::check_connected_socket(
    void
) {
    int32_t ret;
    ESP_LOGD( _tag, "check connect_socket" );
    ret= get_socket_error_code( m_sockHandle );
    if( ret != 0 ) {
        ESP_LOGW( _tag, "socket error %d %s", ret, strerror( ret ) );
    }
    return( ret );
}

S29_UDP_c::S29_UDP_c(
    void
) : m_sockHandle( -1 ) {

    // Initialize transmit socket address
    m_txAddr.sin_family=      AF_INET;
    m_txAddr.sin_addr.s_addr= inet_addr( _CONFIG_UDP_HUB_IP );
    m_txAddr.sin_port=        htons( CONFIG_UDP_TX_PORT );
    memset( &m_txAddr.sin_zero[ 0 ], '\0', sizeof( m_txAddr.sin_zero ) );

    // Initialize receive socket address
    m_rxAddr.sin_family=      AF_INET;
    m_rxAddr.sin_addr.s_addr= inet_addr( "0.0.0.0" );
    m_rxAddr.sin_port=        htons( CONFIG_UDP_RX_PORT );
    memset( &m_rxAddr.sin_zero[ 0 ], '\0', sizeof( m_rxAddr.sin_zero ) );

    return;
}

bool S29_UDP_c::Open(
    void
) {
    //
    bool rv= false;

    do {
        // create udp socket
        m_sockHandle= socket( AF_INET, SOCK_DGRAM, 0 );

        if( m_sockHandle == -1 ) {
            //
            ESP_LOGI( _tag, "Cannot create UDP socket" );
            break;
        }

        uint32_t opt= fcntl( m_sockHandle, F_GETFL, 0 );

        if( opt == -1 ) {
            //
            ESP_LOGI( _tag, "Cannot get UDP socket flags" );
            Close(  );
            break;
        }

        int ret= fcntl( m_sockHandle, F_SETFL, ( opt | O_NONBLOCK ) );

        if( ret == -1 ) {
            //
            ESP_LOGI( _tag, "Cannot set UDP socket flags for non-blocking operation" );
            Close(  );
            break;
        }

        if( bind( m_sockHandle,
                  (struct sockaddr *)&m_rxAddr, sizeof( m_rxAddr ) ) != 0 ) {
            //
            ESP_LOGI( _tag, "Cannot bind UDP socket" );
            Close(  );
            break;
        }
        rv= true;
    }
    while( false );

    return( rv );
}

void S29_UDP_c::Send(
    const char   * const txBuff,
    const size_t            len
) const {
    // send to hub
    sendto( m_sockHandle,
            txBuff, len, 0, (struct sockaddr *)&m_txAddr, sizeof( m_txAddr) );

    return;
}

size_t S29_UDP_c::Receive(
    char   * rxBuff,
    size_t      len
) const {
    // receive from hub
    int32_t rv= -1;

//    ESP_LOGI( _tag, "Receiving UDP into buffer @ 0x%x of the lenght %d", (int)rxBuff, len );


    rv= recvfrom( m_sockHandle, rxBuff, len, 0, NULL, NULL );

//    ESP_LOGI( _tag, "Receive completed rv = %d", rv );

    return( ( rv < 0 ) ? 0 : rv );
}

void S29_UDP_c::Close(
    void
) {
    //
    close( m_sockHandle );
    m_sockHandle= -1;
    return;
}
