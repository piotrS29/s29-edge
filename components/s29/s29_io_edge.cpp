// S29 components
#include <s29_global.h>
#include <s29_MAX30105.h>
#include <s29_Adafruit_BME680.h>
//
//#define FIX_36
#define FORCE_SENS
//#define ENV_SENS
//#define BODY_SENS
//
static const char * _tag= "s29-IO";
//
// S29 components
Adafruit_BME680 S29_IO_EDGE_c::m_bme680;
MAX30105        S29_IO_EDGE_c::m_max30105;
//
MPU9250_DMP     S29_IO_EDGE_c::m_mpu9250;
//
static const gpio_num_t    RED_LED_GPIO=          GPIO_NUM_27;
static const gpio_num_t    ORANGE_LED_GPIO=       GPIO_NUM_32;
static const gpio_num_t    BLUE_LED_GPIO=         GPIO_NUM_33;
//
static const uint16_t      RED_LED_MOD_DIV=       1000;
//
static const uint64_t      GPIO_OUTPUT_PIN_SEL=   ((1ULL<<RED_LED_GPIO)
                                              |    (1ULL<<ORANGE_LED_GPIO)
                                              |    (1ULL<<BLUE_LED_GPIO));
//
#ifdef FIX_36
static const gpio_num_t    BUTTON_GPIO=           GPIO_NUM_17;
static const int           ESP_INTR_FLAG_DEFAULT= 0;
static xQueueHandle gpio_evt_queue=               NULL;
//
static void IRAM_ATTR gpio_isr_handler(
    void* arg
) {
    uint32_t gpio_num= (uint32_t) arg;
    xQueueSendFromISR( gpio_evt_queue, &gpio_num, NULL );
//           puts( "Interrupt\n" );
}
#else
static const gpio_num_t    BUTTON_GPIO=           GPIO_NUM_36;
static const bool          BUTTON_UP=             true;
static const bool          BUTTON_DOWN=           false;
#endif
//
static const uint64_t      GPIO_INPUT_PIN_SEL=    (1ULL<<BUTTON_GPIO);
//
static const gpio_num_t    I2C_MASTER_SCL=        GPIO_NUM_22;
static const gpio_num_t    I2C_MASTER_SDA=        GPIO_NUM_23;
static const uint32_t      I2C_FREQ_HZ=           100000;      /*!< I2C master clock frequency */
//
static const unsigned char     MPU_SENSOR_SEL=   ( INV_XYZ_GYRO | INV_XYZ_ACCEL | INV_XYZ_COMPASS );
static const unsigned short    MPU_GYRO_FSR=     2000;
static const unsigned char     MPU_ACCEL_FSR=    2;
//static const unsigned short    MPU_LPF_RATE=     5;
//static const unsigned short    MPU_RATE=         10;
//static const unsigned short    MPU_MAG_RATE=     10;
static const unsigned short    MPU_LPF_RATE=      5;
static const unsigned short    MPU_RATE=         20;
static const unsigned short    MPU_MAG_RATE=     20;
//
static const uint16_t          ADC_V_REF=        1100; //Use adc2_vref_to_gpio() to obtain a better estimate
//static const adc1_channel_t    ADC1_CHAN_36=     (ADC1_CHANNEL_0);    /*!< ADC1 channel 0 is GPIO36 */
static const adc1_channel_t    ADC1_CHAN_39=     (ADC1_CHANNEL_3);    /*!< ADC1 channel 3 is GPIO39 */
static const adc1_channel_t    ADC1_CHAN_34=     (ADC1_CHANNEL_6);    /*!< ADC1 channel 6 is GPIO34 */
//
static esp_adc_cal_characteristics_t    characteristics;
//
static const adc_atten_t atten = ADC_ATTEN_DB_0;
static const adc_unit_t unit = ADC_UNIT_1;



bool S29_IO_EDGE_c::Open(
    void
) {
    //
    ESP_LOGV( _tag, "S29_IO_EDGE_c::Open" );
    //
    m_ioCount= 0;
    //
    i2c_config_t  i2cCfg= {
        .mode=             I2C_MODE_MASTER,
	    .sda_io_num=        I2C_MASTER_SDA,
	    .sda_pullup_en= GPIO_PULLUP_ENABLE,
        .scl_io_num=        I2C_MASTER_SCL,
        .scl_pullup_en= GPIO_PULLUP_ENABLE,
        {
            .master= {
                .clk_speed=        I2C_FREQ_HZ,
            },
        },
    };
/* In newer version of esp-idf code above should be relaved with the following
    i2c_config_t  i2cCfg= {
        .mode=             I2C_MODE_MASTER,
	    .sda_io_num=        I2C_MASTER_SDA,
        .scl_io_num=        I2C_MASTER_SCL,
	    .sda_pullup_en= GPIO_PULLUP_ENABLE,
        .scl_pullup_en= GPIO_PULLUP_ENABLE,
        {
            .master= {
                .clk_speed=        I2C_FREQ_HZ,
            },
        },
    };
*/
    //
    s29_i2c.init( I2C_NUM_1, &i2cCfg );
    s29_i2c.scan(  );
    //
    gpio_config_t io_conf;
    //
    // Outputs
    //
    // bit mask of the output pins
    io_conf.pin_bit_mask=   GPIO_OUTPUT_PIN_SEL;
    // set as output mode
    io_conf.mode=              GPIO_MODE_OUTPUT;
    // disable interrupt
    io_conf.intr_type=        GPIO_INTR_DISABLE;
    // disable pull-up mode
    io_conf.pull_up_en=     GPIO_PULLUP_DISABLE;
    // disable pull-down mode
    io_conf.pull_down_en= GPIO_PULLDOWN_DISABLE;
    // configure GPIO with the given settings
    gpio_config( &io_conf );
    //
    // Inputs
    //
    // bit mask of the input pins
    io_conf.pin_bit_mask =   GPIO_INPUT_PIN_SEL;
    // set as input mode    
    io_conf.mode=               GPIO_MODE_INPUT;
#ifdef FIX_36
    // interrupt type - on rising, falling, any edge
    io_conf.intr_type=        GPIO_INTR_POSEDGE;
//    io_conf.intr_type=        GPIO_INTR_NEGEDGE;
//    io_conf.intr_type=        GPIO_INTR_ANYEDGE;
#else
    // disable interrupt
    io_conf.intr_type=        GPIO_INTR_DISABLE;
#endif
    // enable pull-up mode
    io_conf.pull_up_en=      GPIO_PULLUP_ENABLE;
    // configure GPIO with the given settings
    gpio_config( &io_conf );

 #ifdef FIX_36
    // create a queue to handle gpio event from isr
    gpio_evt_queue= xQueueCreate( 10, sizeof( uint32_t ) );

    // install gpio isr service
    gpio_install_isr_service( ESP_INTR_FLAG_DEFAULT );
    //hook isr handler for specific gpio pin
    gpio_isr_handler_add( BUTTON_GPIO, gpio_isr_handler, (void *) BUTTON_GPIO );
#endif
    //
#ifdef FORCE_SENS
    // Initialize ADC and characteristics
    adc1_config_width( ADC_WIDTH_BIT_12 );
    adc1_config_channel_atten( ADC1_CHAN_39, ADC_ATTEN_DB_0 );
    adc1_config_channel_atten( ADC1_CHAN_34, ADC_ATTEN_DB_0 );
//    esp_adc_cal_get_characteristics( ADC_V_REF, ADC_ATTEN_DB_0,
//                                     ADC_WIDTH_BIT_12,
//                                     &characteristics );
    esp_adc_cal_value_t val_type =
       esp_adc_cal_characterize(unit, atten, ADC_WIDTH_BIT_12, ADC_V_REF, &characteristics);

#endif
    //
#ifdef ENV_SENS
    // Environment
    bool bret= m_bme680.begin( );
    ESP_LOGI( _tag, "bme680 - begin result = %d", (int)bret );
    //
    m_bme680.setTemperatureOversampling( BME680_OS_8X );
    m_bme680.setHumidityOversampling( BME680_OS_2X );
    m_bme680.setPressureOversampling( BME680_OS_4X );
    m_bme680.setIIRFilterSize( BME680_FILTER_SIZE_3 );
    m_bme680.setGasHeater( 320, 150 ); // 320*C for 150 ms
#endif
    //
#ifdef BODY_SENS 
    // Body
    ESP_LOGI( _tag, "max30105 - begin" );
    m_max30105.begin( s29_i2c, I2C_SPEED_STANDARD, MAX30105_ADDRESS );
    m_max30105.setup(  );
#endif
    //
//#if 0
    // Motion
    ESP_LOGI( _tag, "mpu9250 - begin" );
    m_mpu9250.begin(  );
    //
    ESP_LOGI( _tag, "mpu9250 - setSensors" );
    m_mpu9250.setSensors( MPU_SENSOR_SEL );
    //
    ESP_LOGI( _tag, "mpu9250 - setGyroFSR to %d dps", MPU_GYRO_FSR );
    m_mpu9250.setGyroFSR( MPU_GYRO_FSR );                           // Set gyro to XX dps
    //
    ESP_LOGI( _tag, "mpu9250 - setAccelFSR to +/-%dg", MPU_ACCEL_FSR );
    m_mpu9250.setAccelFSR( MPU_ACCEL_FSR );                         // Set accel to +/-XXg
    //
    ESP_LOGI( _tag, "mpu9250 - setLPF to %d Hz", MPU_LPF_RATE );
    m_mpu9250.setLPF( MPU_LPF_RATE );                               // Set LPF corner frequency to XXHz
    //
    ESP_LOGI( _tag, "mpu9250 - setSampleRate to %d Hz", MPU_RATE );
    m_mpu9250.setSampleRate( MPU_RATE );                            // Set sample rate to XXHz
    //
    ESP_LOGI( _tag, "mpu9250 - setCompassSampleRate to %d Hz", MPU_MAG_RATE );
    m_mpu9250.setCompassSampleRate( MPU_MAG_RATE );                 // Set mag rate to XXHz
//#endif
    //
    return( true );
}
//
void S29_IO_EDGE_c::Read(
    void
) {
//    uint32_t io_num;
   //
//    ESP_LOGV( _tag, "S29_IO_EDGE_c::Read" );
    //
#ifdef FIX_36
    if( xQueueReceive( gpio_evt_queue, &io_num, 0 ) ) {
//        ESP_LOGI( _tag, "GPIO[%d] intr, val: %d", io_num, gpio_get_level( (gpio_num_t)io_num ) );
        S29_VIO_c::vEdgeButtonCountIn( S29_VIO_c::vEdgeButtonCountOut(  ) + 1 );
        S29_VIO_c::vSensorTypeIn( 2 );
    }
#else
    if( BUTTON_DOWN == gpio_get_level( BUTTON_GPIO ) ) {
        //            
        S29_VIO_c::vEdgeButtonCountIn( S29_VIO_c::vEdgeButtonCountOut(  ) + 1 );
        S29_VIO_c::vSensorTypeIn( 2 );
    }
#endif
    //
#ifdef FORCE_SENS
    // Force
//    int32_t edgeV34=  adc1_to_voltage( ADC1_CHAN_34, &characteristics );
//    int32_t center39= adc1_to_voltage( ADC1_CHAN_39, &characteristics );
//    S29_VIO_c::vForceEdgeV34In( edgeV34 );
//    S29_VIO_c::vForceCenterV39In( center39 );
//    if( edgeV34 > 75 || center39 > 75 ) {
//        S29_VIO_c::vSensorTypeIn( 1 );
//    }
#else
    S29_VIO_c::vForceEdgeV34In( 0 );
    S29_VIO_c::vForceCenterV39In( 0 );
    S29_VIO_c::vSensorTypeIn( 0 );
#endif
    //
#ifdef ENV_SENS
    // Environment
    if( ! m_bme680.performReading(  ) ) {
        ESP_LOGI( _tag, "bme680 - failed to perform reading" );
    }
    else {
//        ESP_LOGI( _tag, "Temperature      = %f *C",    m_bme680.temperature );
//        ESP_LOGI( _tag, "Pressure         = %f hPa",   (m_bme680.pressure / 100.0) );
//        ESP_LOGI( _tag, "Humidity         = %f %%",    m_bme680.humidity );
//        ESP_LOGI( _tag, "Gas              = %f KOhms", (m_bme680.gas_resistance / 1000.0) );
////        ESP_LOGI( _tag, "Approx. Altitude = %f m",     m_bme680.readAltitude(SEALEVELPRESSURE_HPA) );
        S29_VIO_c::vEnvTempIn( m_bme680.temperature );
        S29_VIO_c::vEnvPressureIn( (m_bme680.pressure / 100.0) );
        S29_VIO_c::vEnvHumidityIn( m_bme680.humidity );
        S29_VIO_c::vEnvGasIn( (m_bme680.gas_resistance / 1000.0) );
    }
#else
    S29_VIO_c::vEnvTempIn( 36.6 );
    S29_VIO_c::vEnvPressureIn( 1013.25 );
    S29_VIO_c::vEnvHumidityIn( 49.9 );
    S29_VIO_c::vEnvGasIn( 1.29 );
#endif
    //
#ifdef BODY_SENS
    // Body
    uint32_t red=   m_max30105.getRed();
    uint32_t ir=    m_max30105.getIR();
    uint32_t green= m_max30105.getGreen();
    //
//    ESP_LOGI( _tag, "max30105 -> red = 0x%x ir = 0x%x green = 0x%x", red, ir, green );
    S29_VIO_c::vHealthRedLedIn( red );
    S29_VIO_c::vHealthIrLedIn( ir );
    S29_VIO_c::vHealthGreenLedIn( green );
#else
    S29_VIO_c::vHealthRedLedIn( 111 );
    S29_VIO_c::vHealthIrLedIn( 222 );
    S29_VIO_c::vHealthGreenLedIn( 333 );
#endif
    //
//#if 0
    // Motion
    if ( m_mpu9250.dataReady(  ) ) {
        //
//        ESP_LOGI( _tag, "mpu9250 - dataReady" );
        m_mpu9250.update(UPDATE_ACCEL | UPDATE_GYRO | UPDATE_COMPASS);
        //
        if( S29_VIO_c::vEdgeOrangeLedOut(  ) ) {
            //
            S29_VIO_c::vMotionAccelXIn( m_mpu9250.calcAccel( m_mpu9250.ax ) );
            S29_VIO_c::vMotionAccelYIn( m_mpu9250.calcAccel( m_mpu9250.ay ) );
            S29_VIO_c::vMotionAccelZIn( m_mpu9250.calcAccel( m_mpu9250.az ) );
						//
            S29_VIO_c::vMotionGyroXIn( m_mpu9250.calcGyro( m_mpu9250.gx ) );
            S29_VIO_c::vMotionGyroYIn( m_mpu9250.calcGyro( m_mpu9250.gy ) );
            S29_VIO_c::vMotionGyroZIn( m_mpu9250.calcGyro( m_mpu9250.gz ) );
						//
            S29_VIO_c::vMotionMagXIn( m_mpu9250.calcMag( m_mpu9250.mx ) );
            S29_VIO_c::vMotionMagYIn( m_mpu9250.calcMag( m_mpu9250.my ) );
            S29_VIO_c::vMotionMagZIn( m_mpu9250.calcMag( m_mpu9250.mz ) );
        }
        else {
            //
            S29_VIO_c::vMotionAccelXNullIn(  );
            S29_VIO_c::vMotionAccelYNullIn(  );
            S29_VIO_c::vMotionAccelZNullIn(  );
						//
            S29_VIO_c::vMotionGyroXNullIn(  );
            S29_VIO_c::vMotionGyroYNullIn(  );
            S29_VIO_c::vMotionGyroZNullIn(  );
						//
            S29_VIO_c::vMotionMagXNullIn(  );
            S29_VIO_c::vMotionMagYNullIn(  );
            S29_VIO_c::vMotionMagZNullIn(  );
        }
        //  
        S29_VIO_c::vMotionMPUtimeIn( m_mpu9250.time );
    }
    else {
//        ESP_LOGI( _tag, "mpu9250 - !!! DATA NOT READY !!!" );
    }
//#endif
    //
    return;
}
//
void S29_IO_EDGE_c::Write(
    void
) {
    //
//    ESP_LOGV( _tag, "S29_IO_EDGE_c::Write" );
    //
    ++m_ioCount;
    //
    if( m_ioCount % RED_LED_MOD_DIV == 0 ) {
        //
        gpio_set_level( RED_LED_GPIO, !S29_VIO_c::vEdgeRedLedOut(  ) );
        S29_VIO_c::vEdgeRedLedIn( !S29_VIO_c::vEdgeRedLedOut(  ) );
    }
    //
    gpio_set_level( ORANGE_LED_GPIO, !S29_VIO_c::vEdgeOrangeLedOut(  ) );
    gpio_set_level( BLUE_LED_GPIO,   !S29_VIO_c::vEdgeBlueLedOut(  ) );
    //
    return;
}
//
void S29_IO_EDGE_c::Close(
    void
) {
    //
    ESP_LOGV( _tag, "S29_IO_EDGE_c::Close" );
    //
    m_ioCount= 0;
    return;
}
