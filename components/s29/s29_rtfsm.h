#ifndef _S29_RTFSM_H_
#define _S29_RTFSM_H_

typedef enum _s29_fsm_id_e_ {
    //
    s29_MainApp_e,
    s29_UxEdge_e,
}
S29_FSM_ID_e;

class S29_RealTimeFiniteStateMachine_c {

public:

    S29_RealTimeFiniteStateMachine_c( const S29_FSM_ID_e fsmID ) :
        m_fsmID( fsmID ) {
    }

    virtual ~S29_RealTimeFiniteStateMachine_c(  ) {
    }

protected:

    S29_FSM_ID_e m_fsmID;

private:

};

#endif /* of _S29_RTFSM_H_ */
