#include "s29_i2c_dev.h"

bool S29_I2C_DEV_c::write_buf(
	uint8_t   raddr,
	uint8_t * bdata,
	size_t     blen
) {
	size_t wlen= ( blen < S29_MAX_I2C_OUTBOUND_DATA_LEN )
	           ?   blen
	           :   S29_MAX_I2C_OUTBOUND_DATA_LEN;

	m_i2c_data[ 0 ]= raddr;

	for( int i = 0; i < wlen; ++i ) {
	    m_i2c_data[ i + 1 ] = bdata[ i ];
	}

	return( ( m_i2c->write( ( m_dev_addr << 1 ),
	                           &m_i2c_data[ 0 ],
	                               ( wlen + 1 ),
	                                     false ) == 0 )
	        ? true
	        : false );
}

bool S29_I2C_DEV_c::write_reg(
	uint8_t raddr,
	uint8_t rdata
) {
	return(  write_buf( raddr, &rdata, 1 ) );
}

bool S29_I2C_DEV_c::read_buf(
	uint8_t   baddr,
	uint8_t * bdata,
	size_t     blen
) {
	m_i2c_data[ 0 ]= baddr;

	return( (  ( m_i2c->write( ( m_dev_addr << 1 ), &m_i2c_data[ 0 ], 1, true ) == 0 )
	        && ( m_i2c->read( ( ( m_dev_addr << 1 ) | 1 ), bdata, blen, false ) == 0 ) )
	        ? true
	        : false );
}

bool S29_I2C_DEV_c::read_reg(
	uint8_t   raddr,
	uint8_t * rdata
) {
	return( read_buf( raddr, rdata, 1 ) );
}
