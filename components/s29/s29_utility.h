#ifndef _S29_UTILITY_H_
#define _S29_UTILITY_H_

class S29_Utility_c {

public:

    // Current high-res monotonic time
    static double getSystemTime( void );

private:

};

#endif /* of _S29_UTILITY_H_ */
