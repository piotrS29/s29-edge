#ifndef _S29_UDP_H_
#define _S29_UDP_H_

#include <sys/socket.h>

class S29_UDP_c {

public:

    explicit S29_UDP_c(
        void
    );

    ~S29_UDP_c(  ) {  }

    bool Open( void );

    void Send(
        const char   * const txBuff,
        const size_t            len
    ) const;

    size_t Receive(
       char   * rxBuff,
       size_t      len
    ) const;

    void Close( void );

private:

    int32_t            m_sockHandle;
    struct sockaddr_in     m_txAddr;
    struct sockaddr_in     m_rxAddr;

    //get socket error code. return: error code
    int32_t get_socket_error_code( int32_t socket );

    //show socket error code. return: error code
    int32_t show_socket_error_reason( int32_t socket );

    //check connected socket. return: error code
    int32_t check_connected_socket( void );

};

#endif /* of _S29_UDP_H_ */
