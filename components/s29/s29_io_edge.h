#ifndef _S29_IO_EDGE_H_
#define _S29_IO_EDGE_H_
//
#include <s29_MAX30105.h>
#include <s29_Adafruit_BME680.h>
#include <s29_SparkFunMPU9250-DMP.h>
//
class MAX30105;
class Adafruit_BME680;
//
class S29_IO_EDGE_c {
//
public:
    //
    S29_IO_EDGE_c(  ) {
    }
    //
    ~S29_IO_EDGE_c(  ) {
    }
    //
    bool Open(
        void
    );
    //	
    void Read(
        void
    );
    //
    void Write(
        void
    );
    //
    void Close(
        void
    );
    //
private:
    //
    static MAX30105        m_max30105;
    static Adafruit_BME680   m_bme680;
    static MPU9250_DMP      m_mpu9250;
    uint64_t                m_ioCount;
};
//
#endif // of _S29_IO_EDGE_H_
