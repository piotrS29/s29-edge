#ifndef _S29_ESP32_I2C_H_
#define _S29_ESP32_I2C_H_

#include <s29_common.h>

class S29_ESP32_I2C_c {

public:

	S29_ESP32_I2C_c(  ) {  };
	~S29_ESP32_I2C_c(  ) {  };

	void init(
	    i2c_port_t     i2c_num,
	    i2c_config_t *    conf
	);

	void scan(
	    void
	);

	esp_err_t write(
	    uint8_t   addrOnWrite,
	    uint8_t *       bytes,
	    size_t         length,
	    bool              ack
	);

	esp_err_t read(
	    uint8_t    addrOnRead,
	    uint8_t *       bytes,
	    size_t         length,
	    bool              ack
	);

	//static void delay(
	//    uint32_t         msecy
	//);

private:

	i2c_port_t   m_i2c_num;
	i2c_config_t    m_conf;

};

#endif // of _S29_ESP32_I2C_H_
