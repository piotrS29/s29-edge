// S29 components
#include <s29_global.h>

static const size_t DATA_LENGTH=                           512;    /*!<Data buffer length for test buffer*/
static const size_t I2C_MASTER_TX_BUF_DISABLE=               0;    /*!< I2C master do not need buffer */
static const size_t I2C_MASTER_RX_BUF_DISABLE=               0;    /*!< I2C master do not need buffer */
static const size_t I2C_SLAVE_TX_BUF_LEN=      (2*DATA_LENGTH);    /*!<I2C slave tx buffer size */
static const size_t I2C_SLAVE_RX_BUF_LEN=      (2*DATA_LENGTH);    /*!<I2C slave rx buffer size */

static const bool   ACK_CHECK_EN=                         true;     /*!< I2C master will check ack from slave*/
static const bool   ACK_CHECK_DIS=                       false;     /*!< I2C master will not check ack from slave */

void S29_ESP32_I2C_c::init(
	i2c_port_t     i2c_num,
	i2c_config_t *    conf
) {
	size_t       rx_buf_len= I2C_MASTER_TX_BUF_DISABLE;
	size_t       tx_buf_len= I2C_MASTER_RX_BUF_DISABLE;
	int    intr_alloc_flags= 0;

	m_i2c_num= i2c_num;
	memcpy( &m_conf, conf, sizeof( i2c_config_t ) );

	if( conf->mode == I2C_MODE_SLAVE ) {
	    rx_buf_len= I2C_SLAVE_TX_BUF_LEN;
	    tx_buf_len= I2C_SLAVE_RX_BUF_LEN;
	}

	i2c_param_config( i2c_num, conf );

	i2c_driver_install( i2c_num,
	                    conf->mode,
	                    rx_buf_len,
	                    tx_buf_len,
	                    intr_alloc_flags
	                  );

	return;
}



void S29_ESP32_I2C_c::scan(
	void
) {
	int           i;
	esp_err_t espRc;

	printf("\n");
	printf("I2C bus scan\n");
	printf("\n");
	printf("     0  1  2  3  4  5  6  7  8  9  a  b  c  d  e  f\n");
	printf("00:         ");

	for( i = 3; i < 0x78; i++ ) {
	    //
	    i2c_cmd_handle_t cmd = i2c_cmd_link_create(  );
	    i2c_master_start( cmd );
	    i2c_master_write_byte( cmd, ( i << 1 ) | I2C_MASTER_WRITE, 1 /* expect ack */ );
	    i2c_master_stop( cmd );

	    espRc= i2c_master_cmd_begin( m_i2c_num, cmd, 10/portTICK_PERIOD_MS );
	    if( i%16 == 0 ) {
	        printf( "\n%.2x:", i );
	    }
	    if( espRc == 0 ) {
	        printf( " %.2x", i );
	    }
	    else {
	        printf( " --" );
	    }
	    //ESP_LOGD(tag, "i=%d, rc=%d (0x%x)", i, espRc, espRc);
		i2c_cmd_link_delete( cmd );
	}
	printf( "\n\n" );
	return;
}

esp_err_t S29_ESP32_I2C_c::write(
	uint8_t   addrOnWrite,
	uint8_t *       bytes,
	size_t         length,
	bool              ack
) {
	esp_err_t ret= ESP_OK;

	i2c_cmd_handle_t cmd = i2c_cmd_link_create(  );
	i2c_master_start( cmd );
	i2c_master_write_byte( cmd, addrOnWrite, ack );
	if( length ) {
	    i2c_master_write( cmd, bytes, length, ack );
	}
    i2c_master_stop( cmd );
    ret = i2c_master_cmd_begin ( m_i2c_num, cmd, 1000 / portTICK_RATE_MS );
    i2c_cmd_link_delete( cmd );

	return( ret );
}

esp_err_t S29_ESP32_I2C_c::read(
	uint8_t    addrOnRead,
	uint8_t *       bytes,
	size_t         length,
	bool              ack

) {
	esp_err_t ret= ESP_OK;

	if( length ) {

	    i2c_ack_type_t acking = (ack) ? I2C_MASTER_ACK : I2C_MASTER_NACK;

	    i2c_cmd_handle_t cmd = i2c_cmd_link_create(  );
	    i2c_master_start( cmd );
	    i2c_master_write_byte( cmd, addrOnRead, ACK_CHECK_EN );

	    if( length > 1 ) {
	        i2c_master_read( cmd, bytes, length - 1, I2C_MASTER_ACK );
	    }

	    i2c_master_read_byte( cmd, bytes + length - 1, acking );
	    i2c_master_stop( cmd );
	    ret = i2c_master_cmd_begin( m_i2c_num, cmd, 1000 / portTICK_RATE_MS );
	    i2c_cmd_link_delete( cmd );
	}


	return( ret );
}

//void S29_ESP32_I2C_c::delay(
//	uint32_t msec
//) {
//
//}
