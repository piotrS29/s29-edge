#ifndef _S29_VIO_H_
#define _S29_VIO_H_
//
#include <s29_common.h>
//
class S29_VIO_c {
//
public:
    //
    S29_VIO_c(  ) {
    //
    }
    //
    // Current Input
    //
    // Current Output
    //
    // Voltage Input
    //
    inline static void vForceEdgeV34In( const int32_t forceVoltage ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_FORCE_KEY ),
                    EDGE_V34_KEY, cJSON_CreateNumber( forceVoltage ) );
    }
    //
    inline static void vForceCenterV39In( const int32_t forceVoltage ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_FORCE_KEY ),
                    CENTER_V39_KEY, cJSON_CreateNumber( forceVoltage ) );
    }
    //
    // Voltage Output
    //
    // Logic Input
    //
    // Logic Output
    //
    // Numeric Input
    //
    inline static void vSystemTimeIn( const double systemTime ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                EDGE_SYS_TIME_KEY, cJSON_CreateNumber( systemTime ) );
    }
    //
    inline static void vEdgeUDPcountIn( const int64_t counter ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                EDGE_UDP_COUNT_KEY, cJSON_CreateNumber( counter ) );
    }
    //
    inline static void vEdgeButtonCountIn( const int64_t counter ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                EDGE_BUTTON_COUNT_KEY, cJSON_CreateNumber( counter ) );
    }
    //
    inline static void vHubLFScountIn( const int64_t counter ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                HUB_LEFT_FOOT_SWITCH_COUNT_KEY, cJSON_CreateNumber( counter ) );
    }
    //
    inline static void vHubRFScountIn( const int64_t counter ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                HUB_RIGHT_FOOT_SWITCH_COUNT_KEY, cJSON_CreateNumber( counter ) );
    }
    //
    inline static void vEdgeRedLedIn( const bool bval ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                EDGE_RED_LED_KEY, cJSON_CreateBool( bval ) );
    }
    //
    inline static void vEdgeOrangeLedIn( const bool bval ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                EDGE_ORANGE_LED_KEY, cJSON_CreateBool( bval ) );
    }
    //
    inline static void vEdgeBlueLedIn( const bool bval ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                EDGE_BLUE_LED_KEY, cJSON_CreateBool( bval ) );
    }

    //
    inline static void vHealthRedLedIn( const int32_t ledValue ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_HEALTH_KEY ),
                    RED_LED_KEY, cJSON_CreateNumber( ledValue ) );
    }
    //
    inline static void vHealthIrLedIn( const int32_t ledValue ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_HEALTH_KEY ),
                    IR_LED_KEY, cJSON_CreateNumber( ledValue ) );
    }
    //
    inline static void vHealthGreenLedIn( const int32_t ledValue ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_HEALTH_KEY ),
                    GREEN_LED_KEY, cJSON_CreateNumber( ledValue ) );
    }

    //
//    inline static void vEnvTempIn( const int32_t value ) {
    inline static void vEnvTempIn( const float value ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_ENV_KEY ),
                    TEMP_KEY, cJSON_CreateNumber( value ) );
    }
    //
//    inline static void vEnvPressureIn( const int32_t value ) {
    inline static void vEnvPressureIn( const float value ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_ENV_KEY ),
                    PRESSURE_KEY, cJSON_CreateNumber( value ) );
    }
    //
//    inline static void vEnvHumidityIn( const int32_t value ) {
    inline static void vEnvHumidityIn( const float value ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_ENV_KEY ),
                    HUMIDITY_KEY, cJSON_CreateNumber( value ) );
    }
    //
//    inline static void vEnvGasIn( const int32_t value ) {
    inline static void vEnvGasIn( const float value ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_ENV_KEY ),
                    GAS_KEY, cJSON_CreateNumber( value ) );
    }

    //
    inline static void vMotionAccelXNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    ACCEL_X_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionAccelXIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    ACCEL_X_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionAccelYNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    ACCEL_Y_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionAccelYIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    ACCEL_Y_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionAccelZNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    ACCEL_Z_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionAccelZIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    ACCEL_Z_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionGyroXNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    GYRO_X_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionGyroXIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    GYRO_X_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionGyroYNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    GYRO_Y_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionGyroYIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    GYRO_Y_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionGyroZNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    GYRO_Z_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionGyroZIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    GYRO_Z_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionMagXNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    MAG_X_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionMagXIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    MAG_X_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionMagYNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    MAG_Y_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionMagYIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    MAG_Y_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionMagZNullIn( void ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    MAG_Z_KEY, cJSON_CreateNull(  ) );
    }
    //
    inline static void vMotionMagZIn( const float val ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    MAG_Z_KEY, cJSON_CreateNumber( val ) );
    }
    //
    inline static void vMotionMPUtimeIn( const int64_t mpuTime ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem(
                cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ),
                    MPU_TIME_KEY, cJSON_CreateNumber( mpuTime ) );
    }
    //
    inline static void vSensorTypeIn( const int32_t value ) {
        cJSON_ReplaceItemInObjectCaseSensitive(
            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
                EDGE_SENSOR_TYPE_KEY, cJSON_CreateNumber( value ) );
    }
    //
//    inline static void vSensorTypeIn( const char * typeName ) {
//        cJSON_ReplaceItemInObjectCaseSensitive(
//            cJSON_GetObjectItem( dmRoot, DATA_MODEL_KEY ),
//                EDGE_SENSOR_TYPE_KEY, cJSON_CreateString( typeName ) );
//    }
    //
    // Numeric Output
    //
    inline static char * vEdgeIPaddressOut( void ) {
        return( 
            cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    dmRoot, DATA_MODEL_KEY ), EDGE_IP_KEY ) -> valuestring
        );
    }
    //
    inline static int64_t vEdgeUDPcountOut( void ) {
        return( 
            cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    dmRoot, DATA_MODEL_KEY ), EDGE_UDP_COUNT_KEY ) -> valueint
        );
    }
    //
    inline static int64_t vEdgeButtonCountOut( void ) {
        return( 
            cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    dmRoot, DATA_MODEL_KEY ), EDGE_BUTTON_COUNT_KEY ) -> valueint
        );
    }
    //
    inline static int64_t vHubLFScountOut( void ) {
        return( 
            cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    dmRoot, DATA_MODEL_KEY ), HUB_LEFT_FOOT_SWITCH_COUNT_KEY ) -> valueint
        );
    }
    //
    inline static int64_t vHubRFScountOut( void ) {
        return( 
            cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    dmRoot, DATA_MODEL_KEY ), HUB_RIGHT_FOOT_SWITCH_COUNT_KEY ) -> valueint
        );
    }
    //
    inline static bool vEdgeRedLedOut( void ) {
        return( 
            ( cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    dmRoot, DATA_MODEL_KEY ), EDGE_RED_LED_KEY ) -> type == 1 )
            ? false : true
        );
    }
    //
    inline static bool vEdgeOrangeLedOut( void ) {
        return( 
            ( cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    dmRoot, DATA_MODEL_KEY ), EDGE_ORANGE_LED_KEY ) -> type == 1 )
            ? false : true
        );
    }
    //
    inline static bool vEdgeBlueLedOut( void ) {
        return( 
            ( cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    dmRoot, DATA_MODEL_KEY ), EDGE_BLUE_LED_KEY ) -> type == 1 )
            ? false : true
        );
    }
    //
    inline static int64_t vMotionMPUtimeOut( void ) {
        return( 
            cJSON_GetObjectItem(
                cJSON_GetObjectItem(
                    cJSON_GetObjectItem(
                        dmRoot, DATA_MODEL_KEY ), EDGE_MOTION_KEY ), MPU_TIME_KEY ) -> valueint
        );
    }
    //
 };
//
#endif // of _S29_VIO_H_
