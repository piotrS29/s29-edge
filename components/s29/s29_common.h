#ifndef _S29_COMMON_H_
#define _S29_COMMON_H_
//
// Standard enviroment
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <sys/types.h>
//
// Espressif infrastructure
#include <freertos/FreeRTOS.h>
#include <freertos/event_groups.h>
#include <freertos/task.h>
//
#include <esp_event.h>
#include <esp_system.h>
#include <esp_log.h>
#include <esp_adc_cal.h>
//
#include <driver/i2c.h>
#include <driver/gpio.h>
#include <driver/adc.h>
//
#include <cJSON.h>
//
// Project configuration
#include <sdkconfig.h>
//
// Project generated files
#include <_s29_udp.h>
#include <_s29_wifi.h>
//
#define LOCALHOST_IP                           "127.0.0.1"
//
#define DATA_MODEL_KEY                         "dm"
            // items
#define     EDGE_IP_KEY                        "IP"
#define     EDGE_SENSOR_TYPE_KEY               "sType"
#define     EDGE_SYS_TIME_KEY                  "tStamp"
#define     EDGE_UDP_COUNT_KEY                 "uCount"
#define     EDGE_BUTTON_COUNT_KEY              "bCount"
#define     HUB_LEFT_FOOT_SWITCH_COUNT_KEY     "lfsCount"
#define     HUB_RIGHT_FOOT_SWITCH_COUNT_KEY    "rfsCount"
#define     EDGE_RED_LED_KEY                   "ledR"
#define     EDGE_ORANGE_LED_KEY                "ledO"
#define     EDGE_BLUE_LED_KEY                  "ledB"
            // objects
#define     EDGE_FORCE_KEY                     "force"
#define         EDGE_V34_KEY                   "edgeV34"
#define         CENTER_V39_KEY                 "centerV39"
            //
#define     EDGE_HEALTH_KEY                    "health"
#define         RED_LED_KEY                    "redLed"
#define         IR_LED_KEY                     "irLed"
#define         GREEN_LED_KEY                  "greenLed"
            //
#define     EDGE_ENV_KEY                       "env"
#define         TEMP_KEY                       "temp"
#define         PRESSURE_KEY                   "pressure"
#define         HUMIDITY_KEY                   "humidity"
#define         GAS_KEY                        "gas"
            //
#define     EDGE_MOTION_KEY                    "motion"
#define         ACCEL_X_KEY                    "accelX"
#define         ACCEL_Y_KEY                    "accelY"
#define         ACCEL_Z_KEY                    "accelZ"
#define         GYRO_X_KEY                     "gyroX"
#define         GYRO_Y_KEY                     "gyroY"
#define         GYRO_Z_KEY                     "gyroZ"
#define         MAG_X_KEY                      "magX"
#define         MAG_Y_KEY                      "magY"
#define         MAG_Z_KEY                      "magZ"
#define         MPU_TIME_KEY                   "mpuTime"
//
extern cJSON * dmmRoot;
extern cJSON *  dmRoot;
//
#endif // of _S29_COMMON_H_
