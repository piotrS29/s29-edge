_: help

# OS detection and coresponding variable setup
ifeq ($(shell uname),Linux)
    DEFAULT_PORT = /dev/ttyUSB0
    CONFIRM_PORT = ls
    PRESENT_PORT = $(shell $(CONFIRM_PORT) $(DEFAULT_PORT) 2>/dev/null )

    CURRENT_SSID = $(shell cat /etc/wpa_supplicant/wpa_supplicant.conf | grep ssid | awk -F '"' '{ print $$2 }' )
    CURRENT_PSWD = $(shell cat /etc/wpa_supplicant/wpa_supplicant.conf | grep psk  | awk -F '"' '{ print $$2 }' )
    CURRENT_WIP  = $(shell ip addr | grep wlan0 | grep inet | awk '{ print $$2 }'  | awk -F '/' '{ print $$1 }' )

    UPROFILE = .bashrc
    SED = sed
    E =

else ifeq ($(shell uname),Darwin)
    DEFAULT_PORT = /dev/cu.SLAB_USBtoUART
    CONFIRM_PORT = ls
    PRESENT_PORT = $(shell $(CONFIRM_PORT) $(DEFAULT_PORT) 2>/dev/null )

    CURRENT_SSID = 
    CURRENT_PSWD = 
    CURRENT_WIP  = 

    UPROFILE = .profile
    SED = gsed
    E =

else
    DEFAULT_PORT =
    CONFIRM_PORT = chgport | grep 
    PRESENT_PORT = $(shell chgport | grep Silab | awk '{ print $$1 }' )

    CURRENT_SSID = 
    CURRENT_PSWD = 
    CURRENT_WIP  = 

    UPROFILE = .bash_profile
    SED = sed
    E = -e
endif

UDP_H  = ./components/s29/_s29_udp.h
WIFI_H = ./components/s29/_s29_wifi.h

ANAME = edge

help:
	@echo $E "\nUsage: make [target [target] ...]"
	@echo
	@echo $E "MAKE utility targets for S29 EDGE sensor module:"
	@echo
	@echo $E "\t help     \t - print this text"
	@echo $E "\t gen      \t - generate source code"
	@echo $E "\t bin      \t - build binaries"
	@echo $E "\t flash    \t - flash S29 EDGE ESP32 module"
	@echo $E "\t monitor  \t - monitor S29 EDGE ESP32 runtime execution -> Ctrl-] to stop"
	@echo $E "\t cleangen \t - remove generated source code files"
	$(call last_line)

#         ipaddr=$$(ip addr | grep 'inet ' | grep brd | awk '{print $$2}' | awk -F/ '{print $$1}'); \

gen:
	@echo $E "\nGenerating source code ..."
	@var=; \
	 if [ ! -e $(UDP_H) ]; then \
	     echo $E "\nCurrent hub IP address -> $(CURRENT_WIP)"; \
         echo "#ifndef __S29_UDP_H__"                             > $(UDP_H); \
         echo "#define __S29_UDP_H__"                            >> $(UDP_H); \
         echo                                                    >> $(UDP_H); \
         echo "#define _CONFIG_UDP_HUB_IP    \"$(CURRENT_WIP)\"" >> $(UDP_H); \
         echo                                                    >> $(UDP_H); \
         echo "#endif /* of __S29_UDP_H__ */"                    >> $(UDP_H); \
	 fi; \
     echo
#	     hubssid=$(CURRENT_SSID);
#	     hubpswd=$(CURRENT_PSWD);
	@var=; \
	 if [ ! -e $(WIFI_H) ]; then \
	     hubssid=""; \
	     if [ "$$hubssid" = "" ]; then \
             read -p "Enter your hotspot SSID: " hubssid; \
		 else \
	         echo $E "Current hotspot SSID -> $$hubssid"; \
		 fi; \
	     echo; \
	     hubpswd=""; \
	     if [ "$$hubpswd" = "" ]; then \
             read -p "Enter your hotspot Password: " hubpswd; \
		 else \
	         echo $E "Current hotspot PSWD -> $$hubpswd"; \
		 fi; \
	     echo; \
         echo "#ifndef __S29_WIFI_H__"                      > $(WIFI_H); \
         echo "#define __S29_WIFI_H__"                     >> $(WIFI_H); \
         echo                                              >> $(WIFI_H); \
         echo "#define _CONFIG_WIFI_SSID    \"$$hubssid\"" >> $(WIFI_H); \
         echo "#define _CONFIG_WIFI_PSWD    \"$$hubpswd\"" >> $(WIFI_H); \
         echo                                              >> $(WIFI_H); \
         echo "#endif /* of __S29_WIFI_H__ */"             >> $(WIFI_H); \
	 fi

bin: gen
	@echo $E "\nBuilding binaries ..."
	@idf.py build
	@$(call reportNetSetup)

flash:
	@echo $E "\nFlashing S29 EDGE ESP32 module ..."
	@var=; \
     if [ "$(PRESENT_PORT)" = "" ]; then \
	     echo $E "\nNo default port detected to flash ESP32 module"; \
		 read -p "Enter port to use to flash ESP32 module  ----> " port; \
		 $(CONFIRM_PORT) $$port > /dev/null 2>&1; \
		 if [ $$? != 0 ]; then \
		     echo $E "ESP32 module cannot be flashed -> provided $$port port not present\n"; \
         else \
             echo; \
             idf.py -p $$port flash; \
             echo; \
		 fi; \
     else \
		 echo; \
		 idf.py -p $(PRESENT_PORT) flash; \
		 echo; \
	 fi

monitor:
	@echo $E "\nMonitoring S29 EDGE ESP32 runtime execution ...\n"
	@idf.py monitor
	$(call last_line)

cleangen:
	@echo $E "\nRemoving generated source code ..."
	@rm -rf $(UDP_H) $(WIFI_H)
	$(call last_line)

define reportNetSetup
IP=; SSID=; PSWD=; \
IP=$$( cat $(UDP_H) | grep IP | awk -F '"' '{print $$2}' ); \
SSID=$$( cat $(WIFI_H) | grep SSID | awk -F '"' '{print $$2}' ); \
PSWD=$$( cat $(WIFI_H) | grep PSWD | awk -F '"' '{print $$2}' ); \
echo; \
echo $E "S29 system operates in following network environment:\n"; \
echo $E "\tHub destination IP address\t -> $$IP"; \
echo $E "\tWiFi hotspot SSID\t\t -> $$SSID"; \
echo $E "\tWiFi hotspot Password\t\t -> $$PSWD"; \
echo
endef

define last_line
@var=; \
    if [ "$E" = "" ]; then \
        echo; \
    fi
endef
