// Espressif infrastructure
#include <nvs_flash.h>
//
// S29 components
#include <s29_global.h>
#include <s29_wifi.h>
//
static const char * _tag= "s29-Main";
//
extern "C" {
    void app_main( void );
}
//
void app_main(
    void
) {
    //
    char * cJsonPrint;
    //
    nvs_flash_init(  );
    //
    printf( "\nApplication Data Meta Model JSON string: \n\n%s\n\n", dataMetaModel );
    //
    dmmRoot= cJSON_Parse( dataMetaModel ); 
    //
    cJsonPrint= cJSON_Print( dmmRoot );
    printf( "\nApplication Data Meta Model JSON object: \n\n%s\n\n", cJsonPrint );
    free( cJsonPrint );
    //
    printf( "\nApplication Data Model JSON string: \n\n%s\n\n", dataModel );
    //
    dmRoot= cJSON_Parse( dataModel ); 
    //
    cJsonPrint= cJSON_Print( dmRoot );
    printf( "\nApplication Data Model JSON object: \n\n%s\n\n", cJsonPrint );
    free( cJsonPrint );
    //
    s29_WiFi_Setup(  );
    //
    ESP_LOGI( _tag, "Application starting" );
    //
    s29_MainApp.Setup(  );
    s29_MainApp.Loop(  );
    //
    ESP_LOGI( _tag, "Application running" );
    //
    return;
}
